FROM node:18.12-bullseye-slim
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY . .
EXPOSE 3000
RUN chown -R node:node .
USER node
CMD [ "npm", "start" ]