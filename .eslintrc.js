module.exports = {
  env: {
    es2021: true,
  },
  extends: ["prettier"],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ["prettier"],
  rules: {
    'linebreak-style': ['error', process.platform === 'win32' ? 'windows' : 'unix'],
  },
};
